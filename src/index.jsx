import React from "react";
import { hydrate, render } from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";

import Routes from "./routes";

const App = () => {
  return (
    <Router>
      <Routes />
    </Router>
  );
};

const rootElement = document.getElementById("root");
if (rootElement.hasChildNodes()) {
  // for prerendering with puppeteer
  hydrate(<App />, rootElement);
} else {
  render(<App />, rootElement);
}
