/** @jsx jsx */
import { jsx, css } from "@emotion/core";
import { useState } from "react";
import { Link } from "react-router-dom";
import { Helmet } from "react-helmet";

import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import Box from "../../components/Box";
import Button from "../../components/Button";

const settings = {
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
};

const base = css({
  backgroundColor: "blue",
});

const padding = css({
  padding: 16,
});

const redBackground = css({
  backgroundColor: "red",
});

const Home = () => {
  const [bgRed, setBgRed] = useState(false);

  return (
    <div css={[base, padding, bgRed && redBackground]}>
      <Helmet>
        <title>My Home Page</title>
        <meta
          name="description"
          content="This is a react test app with emotion-js"
        />
      </Helmet>

      <div css={padding}>Hello World</div>
      <Button
        onClick={() => setBgRed(!bgRed)}
        label="Click Me"
        variant="secondary"
      />
      <Box>
        <Link to="/about">About Me Screen</Link>
      </Box>

      <h1>
        This is the API url
        {process.env.API_URL}
      </h1>

      <div
        css={css({
          backgroundColor: "orange",
          padding: 30,
          textAlign: "center",
        })}
      >
        <Slider {...settings}>
          <div
            css={css({
              backgroundColor: "purple",
              justifyContent: "center",
            })}
          >
            <h3>1</h3>
          </div>
          <div
            css={css({
              backgroundColor: "purple",
              justifyContent: "center",
            })}
          >
            <h3>2</h3>
          </div>
          <div
            css={css({
              backgroundColor: "purple",
              justifyContent: "center",
            })}
          >
            <h3>3</h3>
          </div>
          <div
            css={css({
              backgroundColor: "purple",
              justifyContent: "center",
            })}
          >
            <h3>4</h3>
          </div>
          <div
            css={css({
              backgroundColor: "purple",
              justifyContent: "center",
            })}
          >
            <h3>5</h3>
          </div>
          <div
            css={css({
              backgroundColor: "purple",
              justifyContent: "center",
            })}
          >
            <h3>6</h3>
          </div>
        </Slider>
      </div>
    </div>
  );
};

export default Home;
