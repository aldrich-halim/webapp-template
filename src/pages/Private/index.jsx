/** @jsx jsx */
import { jsx, css } from "@emotion/core";
import { Link, Redirect } from "react-router-dom";

import Box from "../../components/Box";

const rootElement = document.getElementById("root");
const isCrawler = rootElement.hasChildNodes();

const base = css({
  backgroundColor: "violet",
});

const Private = () => {
  // check auth, only for prerendering
  if (!window.privateKey && !isCrawler) {
    return <Redirect to="/home" />;
  }

  return (
    <div css={base}>
      <h1>This is a private page!</h1>
      <div>Go away...</div>
      <Link
        onClick={() => {
          // remove private key
          window.privateKey = "";
        }}
        to="/home"
      >
        Back
      </Link>
      <Box />
    </div>
  );
};

export default Private;
