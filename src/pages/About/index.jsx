/** @jsx jsx */
import { jsx, css } from "@emotion/core";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

import Box from "../../components/Box";

const base = css({
  backgroundColor: "cyan",
});

const About = ({ history }) => (
  <div css={base}>
    <h1>About Me</h1>
    <Link to="/home">Back</Link>
    <Box />

    <button
      type="button"
      onClick={(e) => {
        e.preventDefault();

        // add some stuff
        window.privateKey = "something";

        // redirect to private
        history.push("/private");
      }}
    >
      Go to private route
    </button>
  </div>
);

About.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

export default About;
