/** @jsx jsx */
import { jsx, css } from "@emotion/core";
import PropTypes from "prop-types";

const primary = css({
  backgroundColor: "blue",
  color: "white",
});

const secondary = css({
  backgroundColor: "yellow",
  color: "neon",
});

const Button = ({ variant, label, ...props }) => {
  return (
    <button
      type="button"
      css={[
        variant === "primary" && primary,
        variant === "secondary" && secondary,
      ]}
      {...props}
    >
      {label}
    </button>
  );
};

Button.propTypes = {
  variant: PropTypes.oneOf(["primary", "secondary"]),
  label: PropTypes.string.isRequired,
};

Button.defaultProps = {
  variant: "primary",
};

export default Button;
