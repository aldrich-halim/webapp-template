/** @jsx jsx */
import { jsx, css } from "@emotion/core";

const base = css({
  backgroundColor: "gold",
  margin: 20,
  borderRadius: 5,
  width: 50,
  height: 50,
});

const Box = ({ ...props }) => <div css={base} {...props} />;

export default Box;
