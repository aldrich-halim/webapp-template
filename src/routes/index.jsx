/** @jsx jsx */
import { jsx, css } from "@emotion/core";
import { Route, Switch, Redirect } from "react-router-dom";

import Home from "../pages/Home";
import About from "../pages/About";
import Private from "../pages/Private";

const Router = () => (
  <div
    css={css({
      padding: 50,
      backgroundColor: window.privateKey ? "black" : "white",
    })}
  >
    <h1 css={css({ color: "gold" })}>Hello</h1>
    <Switch>
      <Route exact path="/home" component={Home} />
      <Route exact path="/">
        <Redirect to="/home" />
      </Route>
      <Route exact path="/about" component={About} />
      <Route exact path="/private" component={Private} />
    </Switch>
  </div>
);

export default Router;
